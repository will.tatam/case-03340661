/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.red61.mavenproject2;

import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author will
 */
public class BeanLookup {

    private InitialContext ctx;

    public BeanLookup(InitialContext ctx) {
        this.ctx = ctx;
    }

    public StatefulBeanLocal getBasketLocal() throws NamingException {
        String url = "java:global/mavenproject2-1-SNAPSHOT/StatefulBean!com.red61.mavenproject2.StatefulBeanLocal";
//		logger.info("Looking up Basket using [" + url + "]");
        return (StatefulBeanLocal) ctx.lookup(url);
    }
}
