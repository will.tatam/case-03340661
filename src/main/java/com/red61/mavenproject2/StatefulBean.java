/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.red61.mavenproject2;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;
import org.apache.log4j.Logger;
//import org.jboss.ejb.client.UUIDSessionID;

/**
 *
 * @author will
 */
@Stateful
@Local(StatefulBeanLocal.class)
@StatefulTimeout(value = 1, unit = TimeUnit.MINUTES)
public class StatefulBean implements StatefulBeanLocal {
    
    private String message;
//    private UUIDSessionID test;
//    private final Logger logger = Logger.getLogger(StatefulBean.class);
    private static final Logger logger = Logger.getLogger(StatefulBean.class.getName());
    
    @PostConstruct
    void init() {
//        test = new UUIDSessionID(UUID.randomUUID());
        logger.info("Start of bean " + hashCode()); //+ " " + test.getUuid().toString());
        message = new Date().toString();
    }

    public String getMessage() {
        return message;
    }
    
    @PreDestroy
    void end() {
        logger.info("End of bean " + hashCode()); // + " " + test.getUuid().toString());
    }
    
}
