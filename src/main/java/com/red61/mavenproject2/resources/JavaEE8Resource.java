package com.red61.mavenproject2.resources;

import com.red61.mavenproject2.BeanLookup;
import com.red61.mavenproject2.StatefulBeanLocal;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
/**
 *
 * @author
 */
@Path("javaee8")
public class JavaEE8Resource {
//
//    @GET
//    public Response ping() {
//        return Response
//                .ok("ping")
//                .build();
//    }
    
    private static Logger logger = Logger.getLogger(JavaEE8Resource.class);

    @GET
    public String message() throws NamingException {
        
        BeanLookup beanLookup = new BeanLookup(new InitialContext());
        StatefulBeanLocal statefulBeanLocal = beanLookup.getBasketLocal();
        return statefulBeanLocal.getMessage();
    }
}
